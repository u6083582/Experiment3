package sample.model;

import javafx.scene.Group;
import sample.display.JfxGroup;
import sample.display.VisualGroup;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by panduKerr on 19/3/17.
 *
 * An area is a collection of obstacles and enemies, used like an arena.
 * In the future it will include textures and NPC's etc.
 * When a particular area is loaded, it is represented by it's collections of GameItems
 * This is meant to hold information about the state of GameItems in the area rather than manipulate them
 *
 *
 */
 class Area extends PhysicsItem {

    private ArrayList <PhysicsItem> StaticPhysicals;
    private ArrayList <Character> characters;
    VisualGroup visualGroup;
    private int autoBoundaryThickness = 10;
    final String name;

    /**
     *
     * @param x - coordinate of the top left corner of Area
     * @param y - coordinate of the top left corner of Area
     * @param lengthX - length from x to the the right-most x coordinate
     * @param lengthY - length from x to the the right-most x coordinate
     * @param setAutoBoundary - True if Area is to produce a boundary of Obstructables around defined area
     * @param protagonist - controlled by the human player
     * @param name - Should be unique. Used to identify area to the user
     */
    Area (int x, int y, int lengthX, int lengthY, Boolean setAutoBoundary, Protagonist protagonist, String name){

        super (x,y,lengthX,lengthY);
        this.name = name;
        StaticPhysicals = new ArrayList<>();
        characters = new ArrayList<>();

        visualGroup = new JfxGroup();

        addCharacter(protagonist);

        if (setAutoBoundary) {
            StaticPhysical ne = new StaticPhysical(x,y,x + lengthX, autoBoundaryThickness);
            StaticPhysical nw = new StaticPhysical(x,y, autoBoundaryThickness,y + lengthY);
            StaticPhysical se = new StaticPhysical(x+lengthX-autoBoundaryThickness,y,autoBoundaryThickness, y+lengthY);
            StaticPhysical sw = new StaticPhysical(x,y + lengthY - autoBoundaryThickness, x + lengthX, autoBoundaryThickness);
            this.addStaticPhysical(ne);
            this.addStaticPhysical(nw);
            this.addStaticPhysical(se);
            this.addStaticPhysical(sw);
        }
    }

    /**
     * Adds i to StaticPhysicals. Adds i's body to visualGroup
     * @return index of i's position in StaticPhysicals
     */
    int addStaticPhysical(StaticPhysical i) {
        StaticPhysicals.add(i);
        visualGroup.add(i.getVisualGroup());
        return StaticPhysicals.indexOf(i);
    }

    /**
     * @return a class that implements the VisualGroup interface
     */
    VisualGroup getVisuals() {
        return visualGroup;
    }


    /**
     * Adds c to characters, adds it's body to visualGroup
     * @return index of c's position in characters
     */
    int addCharacter (Character c) {
        characters.add(c);
        visualGroup.add(c.getVisualGroup());
        return characters.indexOf(c);
    }



    /**
     * Iterates through characters. It calls their makeMove() method.
     */
    void moveCharacters (){
        Iterator<Character> i = characters.iterator();
        int indexNumber = 0;
        while (i.hasNext()) {
            Character character = i.next();
            character.makeMove(characters, StaticPhysicals, indexNumber);

            indexNumber ++;
        }
    }

    /**
     * Assumes protagonist is in index 0 of characters. This is called when the player wants to directly move
     *  the protagonist
     */
    void moveProtagonist(Direction d) {
        characters.get(0).move(d);
    }
    


    



}
