package sample.model;


import javafx.scene.paint.Color;
import sample.Tag;

import java.util.ArrayList;

/**
 * Created by panduKerr on 18/3/17.
 */
public class BadGuy extends Character {

    BadGuy (int x, int y, int squareSize){
        super(x,y,squareSize, 150, 5, 40);
        this.body.setFill(Color.BLACK);
        tags = new Tag[] {Tag.ENEMY};
    }

    private Direction [] moves;
    private int currentMove = 0;
    // this sets whether or not the moves are going backwards
    boolean movesAreIncreasing = true;

    public void setMoves(Direction[] moves) {this.moves = moves;}


    //Moves the object according to its array of present moves. When it reaches the end it works backwards, reversing
    // the order.
    @Override
    public void makeMove (ArrayList <Character> characters, ArrayList <PhysicsItem> obstucts, int thisIndex){
        Direction ourMove = moves[currentMove];
        ourMove = movesAreIncreasing ? ourMove: Util.reverseDirection(ourMove);
        move(ourMove);

            //Changes the index appropriately
        currentMove = movesAreIncreasing ? currentMove + 1 : currentMove - 1;

            //This checks if the index is in range and corrects it.
        boolean checkInRange = (currentMove < moves.length) & (currentMove > -1);
        movesAreIncreasing = checkInRange == movesAreIncreasing;
        if (!checkInRange) currentMove = movesAreIncreasing ? currentMove + 1 : currentMove - 1;


    }

}
