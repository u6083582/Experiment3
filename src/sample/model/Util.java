package sample.model;

import sample.Sign;

import static sample.model.Direction.*;

/**
 * Created by panduKerr on 18/3/17.
 */
public class Util {



    static Direction reverseDirection (Direction d){
        switch (d) {
            case NORTH:
                d = SOUTH;
                break;

            case SOUTH:
                d = NORTH;
                break;

            case EAST:
                d = WEST;
                break;

            case WEST:
                d = EAST;
                break;

        }
        return d;
    }

    //Zero is considered positive
    static public Sign getSign (double a){
        Sign r;
        r = a >= 0 ? Sign.POSITIVE : Sign.NEGATIVE;
        return r;
    }

    public static void main(String[] args) {
        System.out.println("Testing, 1, 2, 3...");
    }

}
