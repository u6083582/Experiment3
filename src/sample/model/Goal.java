package sample.model;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * Created by panduKerr on 18/3/17.
 */
public class Goal extends GameItem {

    Goal (int x, int y){
        super(x,y);

        xSize = 50;
        ySize = 50;
        edgeBuffer = 20;


        this.setX(getX()-xSize - edgeBuffer);
        this.setY(getY()-ySize - edgeBuffer);
        body = new Rectangle(this.getX(),this.getY(),xSize,ySize);
        body.setFill(Color.YELLOW);
        goalGroup.getChildren().add(body);

        haveWon = false;

    }

    private boolean haveWon;
    private int xSize;
    private int ySize;
    private int edgeBuffer;



    Group goalGroup = new Group();
    private Text winText = new Text ("You won!");
    private Rectangle body;


    public Group getGoalGroup() {return goalGroup;}

    public void checkWin (GameItem player) {

        if (distance(player) < 100 & (!haveWon)) {

            winText.setX(200);
            winText.setY(200);
            goalGroup.getChildren().add(winText);

            haveWon = true;

        }

    }


}

