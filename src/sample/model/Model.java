package sample.model;

import sample.display.VisualGroup;

/**
 * Created by panduKerr on 18/3/17.
 */

//This is the main model class. It should be the only class that the other components have to interact with.
//FIXME: make interface for model

public class Model {



    private Goal goal;

    public Goal getGoal() {
        return goal;
    }

    private AreaManager areaManager;
    private Area currentArea;

    private int squareSize;



    public Model(int xSize, int ySize, int squareSize) {

        areaManager = new AreaManager(xSize,ySize,squareSize, new Protagonist(60,60,squareSize, 150, 5, 40));
        System.out.println("gets here");

        goal = new Goal(xSize, ySize);
        this.squareSize = squareSize;
    }




    public void loadArea(int index) {
        currentArea = areaManager.getAreaAtIndex(index);
    }

    public VisualGroup getAreaVisuals (){
        return currentArea.getVisuals();
    }


    public void step (){
        currentArea.moveCharacters();
    }

    public void moveProtagonist(Direction d){
        currentArea.moveProtagonist(d);
    }



    /*
    RUNTIME ADDING GAME ITEMS--------------------------------------------------------------
    */

    public int makeGenericEnemy(int xSquares, int ySquares, Direction[] moves) {
        BadGuy baddie = new BadGuy(xSquares * squareSize, ySquares * squareSize, squareSize);
        baddie.setMoves(moves);
        int index = currentArea.addCharacter(baddie);
        return index;
    }


    public int makeObstacle(int x, int y, int width, int length) {
        StaticPhysical staticPhysical = new StaticPhysical(x, y, width, length);
        int index = currentArea.addStaticPhysical(staticPhysical);
        return index;
    }


}

