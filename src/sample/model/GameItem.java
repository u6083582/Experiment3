package sample.model;

import sample.Tag;

import java.util.Arrays;



/**
 * Created by panduKerr on 18/3/17.
 *
 * The most basic form of something that can be in the game world. The superclass of everything in the game world.
 * Fields represents coordinates in a 2 dimensions.
 * Has an array of tags, the meaning of a tag is contextual.
 */

public class GameItem {
    private double x;
    private double y;
     Tag [] tags;

    public double getX() {return x;}
    public void setX(double x) {this.x = x;}

    public double getY() {return y;}
    public void setY(double y) {this.y = y;}


    public GameItem(double x, double y){
        this.x = x;
        this.y = y;
        tags = new Tag[]{};
    }


    /**
     * @return euclidean distance to other
     */
    double distance (GameItem other) {
        return Math.sqrt(Math.pow(other.getX()-this.x,2) + Math.pow(other.getY()-this.y,2));
    }


    /**
     * Returns true if the queried tag is contained in the tags array
     *
     * @param tag Tag
     * @return - True if this has parameter tag
     */
     boolean searchTag (Tag tag){
        boolean isInTags = false;

        for (int i = 0; i < tags.length ; i++) {
            isInTags = tags[i] == tag;
            if (isInTags) break;
        }

        return isInTags;
    }


    /**
     * Sets tags to tags
     * @param tags - array of Tags
     */
     public void setTags (Tag [] tags){
        this.tags= tags;
    }



    /**
     * Produces a new tags, with length + 1
     * @param tag - Tag to be added
     */
     void addTag (Tag tag) {
        if (!searchTag(tag)){
            Tag [] newTags = new Tag[tags.length+1];

            System.arraycopy(tags,0,newTags,0,tags.length);

            newTags[tags.length] = tag;
            tags = newTags;
        }
    }

    @Override
    public String toString (){
        return ("X: " + Double.toString(x) + ", Y: " + Double.toString(y) + ",  TAGS: " + Arrays.toString(tags) );
    }


}
