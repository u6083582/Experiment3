package sample.model;
import java.util.ArrayList;
import static sample.model.Direction.*;

/**
 * Created by panduKerr on 27/3/17.
 */



/**
 * Holds and instantiates all areas.
 */
public class AreaManager {
    private int xSize;
    private int ySize;
    private int playerLength;
    private Protagonist player;
    private ArrayList<Area> areas;


    /**
     *
     * @param xSize The x-axis length of the map area
     * @param ySize The y-axis length of the map area
     * @param playerLength Standard unit of measurement - it defined as the length of an edge which makes up the player
     *                     square
     * @param player The protagonist which player controls
     */
    AreaManager(int xSize, int ySize, int playerLength, Protagonist player){
        areas = new ArrayList<>();
        this.xSize = xSize;
        this.ySize = ySize;
        this.playerLength = playerLength;
        this.player = player;
        areas.add(makeA0());
    }


    Area getAreaAtIndex(int index) {
        return areas.get(index);
    }

    /**
     *
     * @param xSquares x position, measured in squares from origin
     * @param ySquares y position, measured in squares from origin
     * @param moves an array of Directions. Each Direction is evaluated as a playerLength movement in that direction
     * @return BadGuy
     */
    private BadGuy makeBadGuy(int xSquares, int ySquares, Direction[] moves) {
        BadGuy baddie = new BadGuy((xSquares * playerLength) , (ySquares * playerLength) , playerLength);
        baddie.setMoves(moves);
        return baddie;
    }



    //----------AREA PRODUCTION----------//



    private Area makeA0 () {
        Area a0 = new Area(0,0,xSize,ySize,true, player, "Area One");

//        BadGuy badGuy1 = makeBadGuy(2,2, new Direction [] {SOUTH,SOUTH,SOUTH,SOUTH, SOUTH,SOUTH, EAST, EAST});
//        BadGuy badGuy2 = makeBadGuy(4,5, new Direction [] {EAST,EAST,NORTH,NORTH,EAST,EAST,EAST,EAST});
//        BadGuy badGuy3 = makeBadGuy(3,6, new Direction [] {WEST,WEST, WEST,SOUTH, SOUTH, SOUTH});

        StaticPhysical o1 = new StaticPhysical(200,200,100,10);
//        o1.rotate(90);
//        StaticPhysical o2 = new StaticPhysical(300, 200, 10, 200);

//        a0.addCharacter(badGuy1);
//        a0.addCharacter(badGuy2);
//        a0.addCharacter(badGuy3);

        a0.addStaticPhysical(o1);
//        a0.addStaticPhysical(o2);

        return a0;
    }








}
