package sample.model;

/**
 * Created by panduKerr on 18/3/17.
 */
public enum Direction {

    NORTH, SOUTH, EAST, WEST, NULL, NE, SE, NW, SW

}

