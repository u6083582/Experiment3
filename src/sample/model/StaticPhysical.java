package sample.model;

import javafx.scene.paint.Color;
import sample.Tag;
import sample.display.JfxRectangle;
import sample.display.Visual;

/**
 * Created by panduKerr on 19/3/17.
 */
 class StaticPhysical extends PhysicsItem {
    /**
     * VisualGroup represents the visible state of this'. It is a VisualGroup as this' may be represented as a collection
     *  of Visuals
     */
    private Visual body;

    //The signature includes length and width in case we need to rotate (rather than x start, x end)
    StaticPhysical(int x, int y, int width, int length){
        super(x,y, width, length);
        tags = new Tag[] {Tag.NEUTRAL, Tag.OBSTRUCTABLE};

        body = new JfxRectangle(getPresence().getX(),getPresence().getY(),width,length,Color.DARKGREEN);
        setBody();
        visualGroup.add(body);
    }

    public void rotate(double i) {
        body.rotate(i);
        setX(body.getX());
        setY(body.getY());
        setPresenceRelationship();
        System.out.println("Body's x, y: " + body.getX() + " "+ body.getY());
        System.out.println("StaticPhysical's x, y: " + getX() + " " + getY());
    }


    void setBody (){
        setPresenceRelationship();
        this.body.setX(getPresence().getX());
        this.body.setY(getPresence().getY());
    }

    @Override
    void setPresenceRelationship() {
        this.getPresence().setX(this.getX());
        this.getPresence().setY(this.getY());
    }


}
