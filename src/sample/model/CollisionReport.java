package sample.model;

import sample.Sign;
import sample.model.physics.Vector;

/**
 * Created by panduKerr on 15/4/17.
 */
class CollisionReport {
    //This class is made during a collision, it holds vital information of a collision

    CollisionReport (LinearFunction thisEdge, LinearFunction otherEdge, GameItem intersection, PhysicsItem other){
        this.thisEdge = thisEdge;
        this.otherEdge = otherEdge;
        this.intersection = intersection;
        this.other = other;
    }

    Vector findPerpendicularVector(){
        //This finds a perpendicular vector, but it may be in the opposite direction.
        Vector p = otherEdge.findPerpendicularVector();
        // In order to get the correct direction, draw a line from the centre of other to the intersection,
        //      change that line into a vector, then copy that vector's x and y signs into the perpendicular vector
        Sign x = sample.model.Util.getSign(intersection.getX() - other.getPresence().findCentre().getX());
        Sign y = sample.model.Util.getSign(intersection.getY() - other.getPresence().findCentre().getY());
        p.setSigns(x,y);
        return p;
    }

    GameItem intersection;
    LinearFunction otherEdge;
    //thisEdge refers to the object that invoked the collision report
    LinearFunction thisEdge;
    PhysicsItem other;
}
