package sample.model;



/**
 * Created by panduKerr on 19/3/17.
 *
 * Defines a 2D rectangle on a 2D plane. It's inherited x and y values refer to the coordinates of the upper left hand
 * corner of the rectangle, given that it hasn't been rotated.
 *
 */


//FIXME: allow presences to be rotated.

//Defines a rectangular shape. Has one function: isTouchingOther() which returns a boolean if in contact with another
// presence.
public class Presence extends GameItem{
    public int getxLength() {return xLength;}

    public int getyLength() {return yLength;}

    //Lengths are just that and they are nearly always constant.
    private int xLength;
    private int yLength;

    Presence (double x, double y, int xLength, int yLength) {
        super(x,y);
        this.xLength = xLength;
        this.yLength = yLength;
    }


    GameItem findCentre (){
        double x = getX() + (xLength/2);
        double y = getY() + (yLength/2);
        return (new GameItem(x,y));
    }

    //returns an array of linearFunctions, representing each edge. FIXME: array of Linear Functions
    LinearFunction [] getEdges (){
        LinearFunction lineTop = new LinearFunction(new GameItem(getX(),getY())
                ,new GameItem(getX() + getxLength(), getY()));
        LinearFunction lineRight = new LinearFunction(new GameItem(getX() + getxLength(), getY())
                ,new GameItem(getX() + getxLength(), getY() + getyLength()));
        LinearFunction lineBottom = new LinearFunction(new GameItem(getX(),getY() + getyLength())
                ,new GameItem(getX() + getxLength(), getY() + getyLength()));
        LinearFunction lineLeft = new LinearFunction(new GameItem(getX(),getY())
                ,new GameItem(getX(),getY() + getyLength()));

        return new LinearFunction[] {lineTop,lineBottom,lineLeft,lineRight};
    }


    // Rotation implementation
    // Presence's X, Y are the top left hand corner

    // Instantiate these
    GameItem rightTop;
    GameItem rightBottom;
    GameItem leftBottom;

    void rotate (double degrees){}



}
