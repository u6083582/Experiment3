package sample.model;

import com.sun.istack.internal.Nullable;
import javafx.scene.paint.Color;
import sample.Tag;
import sample.display.JfxRectangle;
import sample.display.Visual;
import sample.model.physics.*;
import java.util.ArrayList;

/**
 * Created by panduKerr on 18/3/17.
 *
 * Character is a class that represents objects that have a visual presence, a physical presence and can move.
 *  They can interact with PhysicsItems
 *
 */


 class Character extends PhysicsItem {

    Visual body;
    private PhysicsAttribute physics;

    /**
     *  magnitude of force each movement command (typically from a player command) applies to the character
     */
    private double moveForceMagnitude = 40;
    private double obstacleHitMagnitude = 20;

    /**
     *
     * @param x coordinate of the centre of the character, for instantiation
     * @param y coordinate of the centre of the character, for instantiation
     * @param playerLength unit of measurement. Defined as edge length of a player
     * @param mass mass of the character
     * @param maxSpeed maximum speed of the character
     * @param moveForce the Force given to the character when move is called
     */
     Character(double x, double y, int playerLength, double mass, double maxSpeed, double moveForce){

        super (x,y,playerLength);

        //Visual construction
        body = new JfxRectangle(this.getX(),this.getY(),40,40,Color.RED);
        setBody();
        getVisualGroup().add(body);

        //Physics attributes
        physics = new PhysicsAttribute(mass, maxSpeed, new GameItem(getX(),getY()),0.95);
        this.moveForceMagnitude = moveForce;

    }


    /**
     * This is function is typically called during player input
     *
     * @param d Direction of force is to be applied in
     */

     void move (Direction d) {

        Vector force;
        switch (d){
            case NORTH:
                force = new Vector(0, -moveForceMagnitude);
                break;

            case SOUTH:
                force = new Vector(0, moveForceMagnitude);
                break;

            case EAST:
                force = new Vector( moveForceMagnitude,0);
                break;

            case WEST:
                force = new Vector( -moveForceMagnitude,0);
                break;

            default:
                force = new Vector(0,0);
        }
        updatePosition(force);
    }



    /**
     *This should be called after any changes to position or physics
     *
     * @param force The force to be applied to the Physics attribute
     */
    private void updatePosition (@Nullable Vector force){
        GameItem newPos = physics.findCurrentPosition(force);

        setY(newPos.getY());
        setX(newPos.getX());

        setBody();
    }


    /**
     * Assumes the Character's coordinates to be current.
     * Updates presence position with setPresenceRelationship()
     * Updates body to match presence position
     */
    private void setBody (){
        setPresenceRelationship();
        body.setX(getPresence().getX());
        body.setY(getPresence().getY());
    }



    /**
     * Determines character's state in the world. Interaction with other PhysicsItems here.
     * It is called every frame.
     *
     * @param characters All other Characters in the world
     * @param obstructs All obstructs in the world
     * @param thisIndex The index of this Character in characters
     */
    void makeMove(ArrayList <Character> characters, ArrayList <PhysicsItem> obstructs, int thisIndex) {
        ArrayList <CollisionReport> collisionPoints = findCollisionPoints(obstructs);

        for (CollisionReport report: collisionPoints) {

            if (report.other.searchTag(Tag.OBSTRUCTABLE)){
                Vector perpendicularVector = report.findPerpendicularVector();
                perpendicularVector = PhysUtil.calculateVector(perpendicularVector
                        , moveForceMagnitude * 2);
                physics.cutVelocity();
                updatePosition(perpendicularVector);
            }

            if (report.other instanceof Character){
                if (report.other instanceof BadGuy){
                    // Check Death here
                }
            }
        }

        updatePosition(null);
    }
}
