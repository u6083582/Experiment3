package sample.model;

import sample.model.physics.PhysicsAttribute;

/**
 * Created by panduKerr on 6/4/17.
 */
public class Protagonist extends Character {

    Protagonist (double x, double y, int squareSize, double mass, double maxSpeed, double moveForce) {
        super (x,y,squareSize,mass,maxSpeed,moveForce);
    }
}
