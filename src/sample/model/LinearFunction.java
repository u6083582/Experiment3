package sample.model;

import sample.Tag;
import sample.model.physics.Vector;

/**
 * Created by panduKerr on 12/4/17.
 */

//Defines a LinearFunction bounded by the xLimits

 class LinearFunction {

    double xLimit1;
    double xLimit2;
    boolean inifinite;

    double gradient;
    double yIntersection;

    //If the line is vertical it is technically not a function
    boolean vertical = false;
    double  yLimit1;
    double yLimit2;


    LinearFunction(GameItem c1, GameItem c2) {

        GameItem coordinate1;
        GameItem coordinate2;

        if (c1.getX() < c2.getX()){
            coordinate1 = c1;
            coordinate2 = c2;
        }
        else {
            coordinate1 = c2;
            coordinate2 = c1;
        }


        xLimit1 = coordinate1.getX();
        xLimit2 = coordinate2.getX();
        inifinite = false;

        if (coordinate1.getX() != coordinate2.getX()) {
            gradient = (coordinate2.getY() - coordinate1.getY()) / (coordinate2.getX() - coordinate1.getX());
            yIntersection = coordinate1.getY() - gradient * coordinate1.getX();
        } else {
            vertical = true;
            yLimit1 = coordinate1.getY();
            yLimit2 = coordinate2.getY();
            xLimit1 = coordinate1.getX();
            xLimit2 = xLimit1;
        }

    }


    //Returns a perpendicular vector parallel to this function. The y vector will be 1 and the x vector is in relation to it
    public Vector findPerpendicularVector () {
        Vector v;

        if (!vertical) {
            v = new Vector(-gradient,1);
        }
        else {
            v = new Vector(1,0);
        }
        return v;
    }



    /*
        FINDING INTERSECTIONS-----------------------------------------------------FINDING INTERSECTIONS
     */

    //Returns the intersections coordinates along with appropriate tags if it is or isn't out of bounds
    GameItem findBoundedIntersection (LinearFunction other) {
        GameItem intersection = findIntersection(other);
        boolean inBounds = false;

        // If no solution then intersect is always out of bounds
        if (!(intersection.searchTag(Tag.NO_SOLUTION))) {

            if (!(this.vertical | other.vertical)) {
            // if neither line is vertical
                inBounds = this.isIntersectWithinXBounds(intersection) && other.isIntersectWithinXBounds(intersection)
                        && doesXBoundaryOverlap(other);

            }

            else if (this.vertical ^ other.vertical){
                // one line is vertical
                LinearFunction vert = this.vertical ? this : other;
                LinearFunction nonVert = this.vertical ? other: this;
                inBounds = vert.isIntersectWithinYBounds(intersection) && nonVert.isIntersectWithinXBounds(intersection);

            }

            else //Both are vertical
            {
                inBounds = this.doesYBoundaryOverlap(other);
            }

        }

        if (inBounds)
            intersection.addTag(Tag.IN_BOUNDS);
        else
            intersection.addTag(Tag.OUT_OF_BOUNDS);


        return intersection;
    }




    private boolean isIntersectWithinXBounds(GameItem intersect){
        return (this.xLimit1 <= intersect.getX() && intersect.getX() <= this.xLimit2);
    }


    private boolean isIntersectWithinYBounds(GameItem intersect){
        return (this.yLimit1 <= intersect.getY() && intersect.getY() <= this.yLimit2)
                || (this.yLimit2 <= intersect.getY() && intersect.getY() <= this.yLimit1);
    }


    private boolean doesYBoundaryOverlap (LinearFunction other) {
        return ((this.yLimit1 <= other.yLimit1) && (other.yLimit1 <= this.yLimit2))
                || ((this.yLimit1 <= other.yLimit2) && (other.yLimit2 <= this.yLimit2));
    }


    private boolean doesXBoundaryOverlap (LinearFunction other) {
        return ((this.xLimit1 <= other.xLimit1) && (other.xLimit1 <= this.xLimit2))
                || ((this.xLimit1 <= other.xLimit2) && (other.xLimit2 <= this.xLimit2));
    }






    //Finds the line's intersection point, treating both lines being unbounded
    GameItem findIntersection(LinearFunction other){
        GameItem r;


        if (!(this.vertical | other.vertical)){
            // Neither lines are vertical


            if (this.gradient != other.gradient) {
                // will have an intersect. Solves for x

                double x = (other.yIntersection - this.yIntersection)/ (this.gradient - other.gradient);
                double y = this.gradient * x + this.yIntersection;
                r = new GameItem(x,y);


            } else if (this.yIntersection == other.yIntersection){
                //These lines are the same

                r = new GameItem(0,0);
                r.setTags(new Tag[] {Tag.INFINITE_SOLUTIONS});


            }
            else {
                //Parallel lines

                r = new GameItem(0,0);
                r.setTags(new Tag[] {Tag.NO_SOLUTION});


            }


        }  else if(this.vertical & other.vertical){
            //Both lines are vertical

            r = new GameItem(0, 0);

            if (this.xLimit1 == other.xLimit1)
                r.setTags(new Tag[] {Tag.INFINITE_SOLUTIONS});

            else r.setTags(new Tag[] {Tag.NO_SOLUTION});


        } else {
            // One line is vertical, other isn't

            LinearFunction verticalLine;
            LinearFunction linearFunction;

            //Finds which line is vertical
            if (this.vertical) {
                verticalLine = this;
                linearFunction = other;

            } else {
                verticalLine = other;
                linearFunction = this;

            }


            double x = verticalLine.xLimit1;
            double y = linearFunction.gradient * x + linearFunction.yIntersection;

            r = new GameItem(x,y);

        }

        return  r;
    }






    //FIXME: DELET THIS
    public static void main(String[] args) {

        LinearFunction line1 = new LinearFunction(new GameItem(1,1), new GameItem(5,5));
        LinearFunction line2 = new LinearFunction(new GameItem(2,4), new GameItem(2.5,3));
        LinearFunction line3 = new LinearFunction(new GameItem(3,4), new GameItem(4,0));

        LinearFunction para1 = new LinearFunction(new GameItem(4,5), new GameItem(5,6));
        LinearFunction same1 = new LinearFunction(new GameItem(2,2),new GameItem(4,4));

        LinearFunction vert1 = new LinearFunction(new GameItem(3,0), new GameItem(3,4));
        LinearFunction vert2  = new LinearFunction(new GameItem(3,0), new GameItem(3,8));

        LinearFunction flat1 = new LinearFunction(new GameItem(2,2),new GameItem(4,2));

        System.out.println(line1.findBoundedIntersection(vert1));


    }

}
