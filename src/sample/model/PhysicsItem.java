package sample.model;

import javafx.scene.Group;
import sample.Tag;
import sample.display.JfxGroup;
import sample.display.VisualGroup;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by panduKerr on 19/3/17.
 * PhysicsItems should be implemented such that the body size is the same as the presence size.
 * PhysicsItems interact with the visible, through it's VisualGroup, and the model-based physical, through its presence
 *  Note, this class does not implement the full suite of physics available, it's up to subclasses to add more physics
 *  functionality as they seem fit.
 */
 abstract class PhysicsItem extends GameItem{
    VisualGroup visualGroup;
    private Presence presence;
    private ArrayList <CollisionReport> previousReports = new ArrayList<>();

    /**
     * Presence represents the model focused representation of the PhysicsItem
     * @return - presence for physics calculations
     */
    Presence getPresence() {return presence;}

    /**
     * VisualGroup represents visible representation of the PhysicsItem
     * @return
     */
    VisualGroup getVisualGroup() {return visualGroup;}

    public void setPresence(Presence presence) {this.presence = presence;}


    /**
     * Produces a square PhysicsItem
     *
     * @param x coordinate of this
     * @param y coordinate of this
     * @param length Edge length of presence
     */
     PhysicsItem(double x, double y, int length){
         super(x,y);
         presence = new Presence(x,y,length,length);
         setPresenceRelationship();
         visualGroup = new JfxGroup();
    }

    /**
     * Produces a rectangular PhysicsItem
     *
     * @param x coordinate of this
     * @param y coordinate of this
     * @param lengthX x edge length of presence
     * @param lengthY y edge length of presence
     */
     PhysicsItem(double x, double y, int lengthX, int lengthY){
        super(x,y);
        presence = new Presence(x,y,lengthX,lengthY);
        setPresenceRelationship();
         visualGroup = new JfxGroup();

     }


    /**
     * Returns a collision report if this is touching another physicsItem's presence.
     * If they aren't touching the CollisionItem will have the tag OUT_OF_BOUNDS.
     * @param other The physicsItem to be tested against
     * @return CollisionReports where this' edge is intersecting other's edge
     */
    ArrayList <CollisionReport> isTouchingOther (PhysicsItem other) {

        Presence otherPresence = other.getPresence();

        LinearFunction [] thisLines = presence.getEdges();

        LinearFunction [] otherLines = other.getPresence().getEdges();


        ArrayList <CollisionReport> collisionReports = new ArrayList<>();

        for (LinearFunction thisLine: thisLines) {

            for (LinearFunction otherLine: otherLines) {
                GameItem intersection = thisLine.findBoundedIntersection(otherLine);
                if (intersection.searchTag(Tag.IN_BOUNDS)){
                    collisionReports.add(new CollisionReport(thisLine,otherLine,intersection, other));
                    other.previousReports.add(new CollisionReport(otherLine,thisLine,intersection,this));
                }
            }
        }
        collisionReports.addAll(previousReports);
        previousReports.clear();
        return  collisionReports;
    }


    /**
     * Finds all points where this Character's edges are intersecting other given PhysicsItem edges
     * @param physicsItems ArrayList of physicItems to check against
     * @return Collision reports at each point of contact
     */
    ArrayList<CollisionReport> findCollisionPoints(ArrayList <PhysicsItem> physicsItems) {
        Iterator<PhysicsItem> i = physicsItems.iterator();

        ArrayList <CollisionReport> contactPoints = new ArrayList<>();

        while (i.hasNext()) {
            ArrayList <CollisionReport> contactWithCurrentObstacle
                    = isTouchingOther(i.next());
            contactPoints.addAll(contactWithCurrentObstacle);
        }
        return contactPoints;
    }


    /**
     * Sets the Presence's coordinate such that the PhysicsItem's coordinates are
     *  in the centre of of the Presence.
     *
     */
    void setPresenceRelationship (){
        double halfX = presence.getxLength() / 2;
        double halfY = presence.getyLength() / 2;

        presence.setX(this.getX() - halfX);
        presence.setY(this.getY() - halfY);

    }

}
