package sample.model.physics;

import sample.Sign;
import sample.Tag;
import sample.model.GameItem;


/**
 * Created by panduKerr on 1/4/17.
 *
 * An extension of GameItem, designed for use in physics. It does not add much to the superclass, but it is intended
 * as a distinction from the coordinate nature of GameItems (generally used for concepts such as velocity, which
 * don't include information about position).
 */
public class Vector extends GameItem {


    public Vector(double x, double y){
        super(x,y);
        setTags(new Tag[] {Tag.PHYSICS,Tag.VECTOR});
    }

    /**
     * This changes sign(positivity) of x and y values.
     * @param x the new sign of x value
     * @param y the new sign of y value
     */
    public void setSigns(Sign x, Sign y){
        Sign xSign = sample.model.Util.getSign(getX());
        Sign ySign = sample.model.Util.getSign(getY());

        double newX = xSign == x ? getX(): getX() * - 1;
        double newY = ySign == y ? getY(): getY() * - 1;

        setX(newX);
        setY(newY);
    }


    /**
     * A wrapped call PhysUtil.calculateMagnitude(). This can be replaced with PhysUtil.calculateMagnitude(this')
     * @return scalar magnitude of this'
     */
    public double getMagnitude (){
        return PhysUtil.calculateMagnitude(getX(),getY());
    }

}
