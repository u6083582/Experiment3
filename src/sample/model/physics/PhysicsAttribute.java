package sample.model.physics;

import com.sun.istack.internal.Nullable;
import sample.model.GameItem;

/**
 * Created by panduKerr on 1/4/17.
 *
 * A PhysicsAttribute is the implementation of a rigid body on a 2D plane
 * A PhysicsAttribute is an object with physics related characteristics: mass, velocity, friction, position
 * The PhysicsAttributes inherited x and y fields refers to the velocity of the attribute.
 *
 * PhysicAttributes are primarily interacted with by adding force vectors through findCurrentPosition(). The friction
 * field ensures that the physics attribute's resting state is still.
 */
public class PhysicsAttribute  extends Vector{
    private double mass;
    private double maxSpeed;
    private GameItem position;
    double friction;


    /**
     * @param mass - used for calculating the effect of forces on this'
     * @param maxSpeed - maximum scalar speed that this object can travel at
     * @param position - coordinate position for instantiation
     * @param friction - 0 - 1 double that is multiplied to final speed every frame. The closer to zero the greater
     *                 the effect
     */
    public PhysicsAttribute (double mass, double maxSpeed,  GameItem position, double friction){
        super(0,0);
        this.mass = mass;
        this.maxSpeed = maxSpeed;
        this.position = position;
        this.friction = friction;
    }


    /**
     * Updates the state of the PhysicsAttribute. An optional force vector is transformed into acceleration. This is
     *  then added to velocity.
     * @param force - Adds a force just for this frame. This is realised as adding the resultant acceleration to the
     *              velocity
     * @return - updated position after calculations
     */
    public GameItem findCurrentPosition (@Nullable Vector force){
        GameItem newPosition;

        if (force != null) {
            Vector acceleration = new Vector(force.getX() / mass, force.getY() / mass);
            newPosition = addAcceleration(acceleration);
        }
        else
            newPosition = addAcceleration(new Vector(0,0));

        return newPosition;
    }


    /**
     *This adds a scalar magnitude to the current velocity. It relies on this' current velocity as a marker for
     * the direction of the resultant velocity. Consequentially, if this' isn't moving, the direction will be undefined
     * and a zero force vector will be added instead. In the future this may be resolved via a current direction
     * or last known direction attribute.
     * @param magnitude - scalar representing the magnitude of the to-be added velocity
     * @return - updated position after calculations
     */
    public GameItem findCurrentPosition (double magnitude){
        double speed = this.getMagnitude();
        Vector acceleration;

        if (speed != 0)
            acceleration = PhysUtil.calculateVector(this, magnitude/mass);
        else
            acceleration = new Vector(0,0);

        return addAcceleration(acceleration);
    }


    /**
     * Sets velocity in x, y directions to 0. Called to stop movement of this'.
     */
    public void cutVelocity (){
        this.setX(0);
        this.setY(0);
    }


    /**
     * Methods that call addAcceleration are to calculate the acceleration vector to be added. This method adds the x
     * and y components of the acceleration vector to this' velocity
     * @param acceleration - Vector calculated by calling method
     * @return - Updated x,y position in 2D space
     */
    private GameItem addAcceleration (Vector acceleration){

        double velocityX = friction*(getX() + acceleration.getX());
        double velocityY = friction*(getY() + acceleration.getY());

        //rounds velocity to zero if it gets too small.
        velocityX = 0.1 > Math.abs(velocityX) ? 0 : velocityX;
        velocityY = 0.1 > Math.abs(velocityY) ? 0 : velocityY;

        double newSpeed  = PhysUtil.calculateMagnitude(velocityX,velocityY);

        //if the newSpeed is faster than maxSpeed we set assign velocityX and velocityY to magnitude of maxSpeed,
        // preserving the original direction.
        if (newSpeed > maxSpeed) {
            Vector maxVelocity = PhysUtil.calculateVector(new Vector(velocityX,velocityY),maxSpeed);
            velocityX = maxVelocity.getX();
            velocityY = maxVelocity.getY();
        }

        setX(velocityX);
        setY(velocityY);

        //Updates position
        position.setX(position.getX() + getX());
        position.setY(position.getY() + getY());

        return position;
    }


    @Override
    public String toString (){
        String s = "DISPLACEMENT x: " + position.getX() + ", y: " +position.getY();
        s += "    VELOCITY x: " + getX() + ", y: " + getY() + '\n';
        return s;
    }

}
