package sample.model.physics;

import sample.model.GameItem;

/**
 * Created by panduKerr on 3/4/17.
 *
 * A collection of calculation methods for use in dealing with vectors on a 2D plane.
 */
public class PhysUtil {

    /**
     * Magnitude calculated using rearranged pythagoras theorem.
     * @return magnitude of given vector. Is a scalar and will always be >= 0. Is = 0 when vector is a 0 vector
     */
    public static double calculateMagnitude (GameItem vector){
        return Math.sqrt((Math.pow(vector.getX(),2) + Math.pow(vector.getY(),2)));
    }

    /**
     * Magnitude calculated using rearranged pythagoras theorem.
     * @param x - x vector
     * @param y - y vector
     * @return magnitude of given vector. Is a scalar and will always be >= 0. Is = 0 when x, y are 0
     */
    public static double calculateMagnitude (double x, double y){
        return  Math.sqrt((Math.pow(x,2) + Math.pow(y,2)));
    }


    /**
     * Produces a vector in direction of referenceVector, with a magnitude of magnitude. Finds magnitude's size in
     * relation to referenceVector's magnitude and sets x and y components in relation to this.
     * @param referenceVector used only as a reference for desired direction. Does not get modified. If this is a 0
     *                        vector, then the return vector will aso be 0
     * @param magnitude scalar representing the desired new vector's magnitude
     * @return a new vector with magnitude magnitude, in direction of referenceVector
     */
    public static Vector calculateVector (Vector referenceVector, double magnitude){

        double referenceMagnitude = referenceVector.getMagnitude();
        double ratio = magnitude / referenceMagnitude;
        double newX = ratio * referenceVector.getX();
        double newY = ratio * referenceVector.getY();

        return new Vector(newX,newY);
    }

    /**
     *Increases referenceVector by magnitude, with magnitude being converted to a vector in the direction of
     * referenceVector.
     * @param referenceVector - If zero vector, this function will return zero vector, regardless of magnitude
     */
    public static Vector calculateAndAddVector (Vector referenceVector, double magnitude){
        return addVectors(calculateVector(referenceVector, magnitude),referenceVector);
    }

    /**
     * @return vector addition of the parameters
     */
    public static Vector addVectors (Vector vector1, Vector vector2){
        return new Vector(vector1.getX() + vector2.getX(), vector1.getY() + vector2.getY());
    }


}
