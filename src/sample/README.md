##Structure
The structure is Control - Model - Display.

### Display
Information about the display is stored in the display module. The model
may only be able to interact with display through the Visual interfaces.

Display is based off of a JavaFx tree model, with nodes containing other
nodes and visible objects.




##DICTIONARY
Definitions of commonly used terms used in code

* visible - something that can be, and is intended to be, seen by the
user

* body - the visible entity of an object for representation to the real
world

* presence - The physical area defined within the model. It can interact
with other presences via the physics system. Is normally closely tied 
with a body.

* distanceUnit - standard unit of measurement. Is defined in relation
to the the body of the protagonist. If the body is is a square, 
distanceUnit is a length of an edge. If the body is a circle it is the 
diameter of body.

* player - refers to the user

* protagonist - The GameItem controlled directly by the human player

