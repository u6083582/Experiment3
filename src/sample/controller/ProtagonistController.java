package sample.controller;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import sample.model.Direction;
import sample.model.Model;

/**
 * Created by panduKerr on 18/3/17.
 */
class ProtagonistController implements EventHandler <KeyEvent>{

    private Model model;

    ProtagonistController (Model model){
        this.model=model;
    }

    boolean up = false;
    boolean down = false;
    boolean left = false;
    boolean right = false;


    //Handles keyboard input
    @Override
    public void handle (KeyEvent c){


        if (c.getEventType() == KeyEvent.KEY_TYPED) {



            switch (c.getCharacter()) {

                case "w":
                    up = true;
                    break;

                case "s":
                    down =true;
                    break;

                case "a":
                    left = true;
                    break;

                case "d":
                    right = true;
                    break;

            }
        }

        else if (c.getEventType() == KeyEvent.KEY_RELEASED){
            switch (c.getCode()){
                case W:
                    up = false;
                    break;

                case S:
                    down =false;
                    break;

                case A:
                    left = false;
                    break;

                case D:
                    right = false;
                    break;
            }
        }


    }



}
