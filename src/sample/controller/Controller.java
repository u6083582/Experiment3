package sample.controller;


import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import sample.model.Direction;
import sample.model.Model;

//Handles the IO of game (through its protagonistController).
// also handles the clock and time (through the constructor's gameTimeline and gameKeyFrame)
public class Controller {
    Model model;
    Timeline gameTimeLine;
    KeyFrame gameKeyFrame;


    //FIXME: add classes for setOnKeyPressed and setOnKeyReleased
    //  good reference: http://stackoverflow.com/questions/23052257/multiple-key-press-on-javafx-scene


    ProtagonistController protagonistController;


    public ProtagonistController getProtagonistController() {return protagonistController;}


    public Controller (Model m, double delay) {
        this.model = m;
        protagonistController = new ProtagonistController(model);

        gameKeyFrame = new KeyFrame(Duration.millis(delay),
                ae ->{

                    model.step();
                    movePlayer();

                });

        gameTimeLine = new Timeline(gameKeyFrame);
        gameTimeLine.setCycleCount(Animation.INDEFINITE);
        gameTimeLine.play();


    }

    void movePlayer (){
        if (protagonistController.up) model.moveProtagonist(Direction.NORTH);

        if (protagonistController.down) model.moveProtagonist(Direction.SOUTH);

        if (protagonistController.left) model.moveProtagonist(Direction.WEST);

        if (protagonistController.right) model.moveProtagonist(Direction.EAST);


    }

}
