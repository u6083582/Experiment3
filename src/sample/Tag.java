package sample;

/**
 * Created by panduKerr on 27/3/17.
 *
 * Quick way to add meaning to any Class to include it.
 */
public enum Tag {
    ENEMY, NEUTRAL, OBSTRUCTABLE, PHYSICS, VECTOR, NO_SOLUTION, INFINITE_SOLUTIONS, OUT_OF_BOUNDS, IN_BOUNDS
    , PROTAGONIST
}
