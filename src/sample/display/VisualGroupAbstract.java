package sample.display;

/**
 * Created by panduKerr on 15/5/17.
 * An abstract class that implements VisualGroup: it ensures package private methods.
 * Any class wishing to implement VisualGroup should instead extend this class.
 */
abstract class VisualGroupAbstract implements VisualGroup {
    /**
     * @return the visible object wrapped by a subclass. This object should only be able to be read by the display
     *  module
     */
    abstract Object getWrappedVisible ();
}
