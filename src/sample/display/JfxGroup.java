package sample.display;

import javafx.scene.Group;
import javafx.scene.Node;
import jdk.nashorn.internal.runtime.ECMAException;

/**
 * Created by panduKerr on 14/5/17.
 *
 * Javafx implementation of the VisualGroup interface.
 */
public class JfxGroup extends VisualGroupAbstract {
    Group group;

    public JfxGroup() {
        group = new Group();
    }

    /**
     * Only access to wrapped group
     * @return javafx Group
     */
    Group getWrappedVisible() {
        return group;
    }

    /**
     * Adds the object returned by v.getVisual to the group.
     *  Note: does not actually save a reference to v
     *
     * @param v - v must extend visualAbstract. v.getVisual must return a javafx.scene.Node
     */
    public void add (Visual v) {
        VisualAbstract va = (VisualAbstract) v;
        Node n = (Node) va.getWrappedVisible();
        group.getChildren().add(n);
    }

    /**
     * @param v - Must extend VisualGroupAbstract - v.getVisual must return a javafx.scene.Node
     */
    @Override
    public void add(VisualGroup v) {
        VisualGroupAbstract vga = (JfxGroup) v;
        Group g = (Group) vga.getWrappedVisible();
        group.getChildren().add(g);
    }

    @Override
    public String toString() {
        return "Hi this is a JfxGroup";
    }

    public void clear (){
        group.getChildren().clear();
    }

}
