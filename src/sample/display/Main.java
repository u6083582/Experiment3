package sample.display;

import javafx.application.Application;

//TranslateTransition is useful

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.controller.Controller;
import sample.model.Model;


/**
 *
 *  This application implements Controller - View - Model
 *  This package is a javafx implementation of the display module.
 *
 *  Implemented as a Singleton
 *
 */

public class Main extends Application {

    Controller controller;
    Model      model;
    final int sizeX = 1000;
    final int sizeY = 800;
    final int squareSize = 40;

    @Override
    public void start(Stage primaryStage) throws Exception{

        // These are the other two components that make up this game
        model      = new Model(sizeX,sizeY, 40);
        controller = new Controller(model,10);


        // Setting up the window code
        Group root = new Group();
        primaryStage.setTitle("Experiment_3");
        Scene scene_main = new Scene(root, sizeX, sizeY);
        primaryStage.setScene(scene_main);




        model.loadArea(0);



        root.getChildren().add(model.getGoal().getGoalGroup());


        //Get current area's visual
        JfxGroup vga = (JfxGroup) model.getAreaVisuals();
        root.getChildren().add(vga.getWrappedVisible());



        // This links the application to the getProtagonistController for control
        scene_main.setOnKeyTyped(controller.getProtagonistController());
        scene_main.setOnKeyReleased(controller.getProtagonistController());





        primaryStage.show();

    }



}
